{{--create.blade.php--}}
@extends('layouts.app')
@section('content')
    <div class="card">
        <h5 class="card-header">Convert to Short URL</h5>
        <div class="card-body">
    <form method="post" action="{{ url('/')}}">
        @csrf
        <div class="form-group">
            <label>Long URL</label>
            <input type="text" name="long_url" class="form-control">
        </div>
        <button type="submit" class="btn btn-dark">CREATE SHORT URL</button>
    </form>

@endsection

