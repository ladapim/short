{{--create.blade.php--}}
@extends('layouts.app')
@section('content')
    <div class="card">
        <h5 class="card-header">List</h5>
        <div class="card-body">
    @if(count($todos)>0)
        @foreach($todos as $todo)
        <div>
            <p>{{$todo->created_at}}</p>
            <a href="{{url($todo->long_url)}}">
                <h3 class="text-warning">{{$todo->long_url}}</h3>
                <p class="badge badge-pill badge-warning">{{$todo->view}} views</p>
            </a>
            <input id="todo{{$todo->id}}" class="form-control" type="text" value="http://www.short.local/t/{{$todo->short_url}}" readonly>
            <br>
            <button onclick="copy(this)" value="{{$todo->id}}" type="button" class="btn btn-dark">Copy</button>

        </div>
       <hr>
    @endforeach
    @endif
    <script>
        function copy(clickedBtn) {
            var id = clickedBtn.value;
            var copyText = document.querySelector('#todo'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied' + copyText.value);

        }
    </script>
@endsection
